# LibreTranslate API

## Your own translation API

### Install

```
$ git clone --recurse-submodules https://gitlab.com/manalejandro/libretranslate-api
$ cd libretranslate-api/
$ docker compose pull libretranslate-api
$ docker compose build libretranslate
```

### Run

```
$ docker compose up -d
```

### Usage

```
$ curl "http://libretranslate-api:5080/?target=es&text=help"
```

### Latest docker images

Pull from [gitlab.com registry](https://gitlab.com/manalejandro/libretranslate-api/container_registry/)

### License

    MIT
