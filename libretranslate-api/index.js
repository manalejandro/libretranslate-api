const http = require('http')

http.createServer(async (req, res) => {
    const params = new URLSearchParams(req.url.replace(/^\//, ''))
    if (params.has('text') && params.has('target')) {
        const response = await fetch("http://libretranslate:5000/translate", {
            method: "POST",
            body: JSON.stringify({
                q: params.get('text'),
                source: "auto",
                target: params.get('target'),
                format: "text",
                api_key: ""
            }),
            headers: { "Content-Type": "application/json" }
        }),
            result = await response.json()
        res.writeHead(200, { 'Content-Type': 'text/plain; charset=UTF-8' })
        res.end(result.translatedText)
    } else {
        res.statusCode = 404
        res.end()
    }
}).listen(3000)

